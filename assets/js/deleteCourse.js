let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

fetch(`https://reynan-capstone2.herokuapp.com/api/courses/${courseId}`, {
method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	alert("Course Successfully Archived")
	window.location.replace("./courses.html")
})