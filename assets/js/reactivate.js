let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");
let isActive = document.querySelector("#isActive");

fetch(`https://reynan-capstone2.herokuapp.com/api/courses/activate/${courseId}`, {
method: 'DELETE',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	alert("Course is now Available")
	window.location.replace("./courses.html")
})