//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");
//retrieve the userId stored in our local storage
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
	
fetch(`https://reynan-capstone2.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	let adminUser = localStorage.getItem("isAdmin")
	if(adminUser == "true" || !adminUser){
		enrollButton = null
	}else{
			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`


	let enrollButton = document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		fetch('https://reynan-capstone2.herokuapp.com/api/users/enroll', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//enrollment is successful
				alert("Thank you for enrolling! See you!")
				window.location.replace('./courses.html')
			}else{
				alert("Enrollment failed")
			}
		})

	})
	
	}

})

// // let token = localStorage.getItem("token");

// let courseIds = []
// let courseNames = []

// fetch('http://localhost:3000/api/courses', {
// 	method: 'GET',
// 	headers: {
// 		'Content-Type': 'application/json',
// 		'Authorization': `Bearer ${token}`
// 	}
// })
// .then(res => res.json())
// .then(data => {

// 	for(let i = 0; i < data.enrollments.length; i++){
// 		courseIds.push(data.enrollments[i].courseId)
// 	}

// 	fetch('http://localhost:3000/api/details')
// 	.then(res => res.json())
// 	.then(data => {

// 		for(let i = 0; i < data.length; i++){

// 			if(courseIds.includes(data[i]._id)){
// 				courseNames.push(data[i].name)
// 			}
// 	}

// 	let coursesList = courseNames.join("\n")
//  	console.log(courseNames)
// 		console.log(coursesList)
// })
// })

