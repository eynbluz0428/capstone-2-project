let token = localStorage.getItem("token");

let courseIds = []
let courseNames = []
let firstName = document.querySelector("#firstName");
// let userId = params.get('userId')
fetch('https://reynan-capstone2.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	for(let i = 0; i < data.enrollments.length; i++){
		courseIds.push(data.enrollments[i].courseId)
	}

	fetch('https://reynan-capstone2.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {

		for(let i = 0; i < data.length; i++){

			if(courseIds.includes(data[i]._id)){
				courseNames.push(data[i].name)
			}
	}

	let coursesList = courseNames.join(",\n")
 	 //Course names are here in coursesList
  		// console.log(courseNames)
		// console.log(coursesList)

			fetch('https://reynan-capstone2.herokuapp.com/api/users/details', {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {

					let profileContainer = document.querySelector("#profileContainer");

				profileContainer.innerHTML = `<div id="card-parent" class="col-md-6 my-3">
				<div class="card-profile">
				<img class="container" id="userLogo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcShm-ah4zFZ9ey6eymv0zH2O0Ggug4jKdbF2w&usqp=CAU" alt="...">
					<div>
						<h4 class="card-title-profile text-center">Enrollees Profile</h4>
						<h3 class="card-text-profile text-left">Firstname: ${data.firstName}</h3>
						<h3 class="card-text-profile text-left">Lastname: ${data.lastName}</h3>
						<h3 class="card-text-profile text-left">Registered Email: ${data.email}</h3>
						<h3 class="card-text-profile text-left">Mobile Number: ${data.mobileNo}</h3>
						<h3 class="card-text-profile text-left">Enrollments: ${coursesList}</h3>
					</div>
				</div>`
		})
	})
})